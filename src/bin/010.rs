mod sequence;

fn main() {
    println!("{:?}", sequence::primes_to(2000000).iter().fold(0, |acc, x| acc + x));
}
