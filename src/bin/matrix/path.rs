// Counts number of distinct routes from (0, 0) to (width, height)
// where coordinates can only be incremented one direction at a time.

// The width and height are of the squares between paths, eg:
//
//               ┏━━┳━━┓
//  2 * 2 grid:  ┣━━╋━━┫  ->  6 unique paths
//               ┗━━┻━━┛

pub fn count_direct_paths(width: usize, height: usize) -> usize {
    let mut grid: &mut Vec<Vec<usize>> = &mut vec![vec![0; width + 1]; height + 1];

    fn paths_to_end(grid: &mut Vec<Vec<usize>>, width: usize, height: usize, x: usize, y: usize) -> usize {
        if grid[x][y] == 0 {
            if x == width || y == height {
                grid[x][y] = 1;
            } else  {
                grid[x][y] = paths_to_end(grid, width, height, x+1, y) +
                    paths_to_end(grid, width, height, x, y+1);
            }
        }
        grid[x][y]
    }

    paths_to_end(grid, width, height, 0, 0)
}
