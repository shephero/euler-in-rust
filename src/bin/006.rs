fn main() {
    let mut sum_of_squares = 0;
    let mut sum = 0;
    for i in 1..101 {
        sum += i;
        sum_of_squares += i * i;
    }
    let square_of_sums = sum * sum;
    println!("{}", square_of_sums - sum_of_squares);
}
