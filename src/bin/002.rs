mod sequence;

fn main() {
    let mut answer: u64 = 0;
    for i in sequence::fibonacci() {
        if i > 4000000 { break; }
        if i % 2 == 0 {
            answer += i;
        }
    }
    println!("{}", answer);
}
