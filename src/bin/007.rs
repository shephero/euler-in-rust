mod sequence;

fn main() {
    for i in sequence::prime_iter().enumerate() {
        if i.0 == 10000 {
            println!("{}", i.1);
            break;
        }
    }
}
