// I was surprised this runs as fast as it does.
mod numplay;

fn main() {
    let mut largest = 0;
    for i in 100..1000 {
        for j in i..1000 {
            let product = i * j;
            if numplay::palindrome(product, 10) && product > largest {
                largest = product;
            }
        }
    }
    println!("{}", largest);
}
