// It's a shame rust doesn't (as of yet) support tail call optimisation.
mod factor;

fn main() {
    let result = factor::highest_prime(600851475143, 2, 1);
    println!("{}", result);
}
