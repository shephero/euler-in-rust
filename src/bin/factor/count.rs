// Returns amount of factors
// eg. count(28) -> 6
pub fn count(num: u64) -> u64 {
    let mut divisors = 0;
    for i in 1..((((num as f64).sqrt()).floor() as u64) + 1) {
        if num % i == 0 {
            divisors += 2;
            if i * i == num {divisors -= 1};
        }
    }
    divisors
}
