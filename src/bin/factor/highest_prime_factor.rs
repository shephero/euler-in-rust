// returns the highest prime factor of a number
pub fn highest_prime(num: u64, start: u64, highest: u64) -> u64 {
    if num % start == 0 {
        return highest_prime(num / start, start, start);
    } else if num <= start {
        return highest;
    } else {
        return highest_prime(num, start + 1, highest);
    }
}
