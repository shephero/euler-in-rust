pub mod highest_prime_factor;
pub use self::highest_prime_factor::highest_prime;

pub mod count;
pub use self::count::count;
