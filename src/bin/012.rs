mod sequence;
mod factor;

fn main() {
    for i in sequence::triangular() {
        if factor::count(i) > 500 {
            println!("{:?}", i);
            break;
        }
    }
}
