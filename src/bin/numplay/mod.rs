pub mod palindrome;
pub use self::palindrome::palindrome;

pub mod to_words;
pub use self::to_words::to_words;
