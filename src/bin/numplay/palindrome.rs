// returns true if a number is the same forwards as
// backwards for the given radix (base)
pub fn palindrome(num: u64, radix: u64) -> bool {
    let mut copy = num;
    let mut rev = 0;
    while copy > 0 {
        rev = rev * radix + (copy % radix);
        copy /= radix;
    }
    num == rev
}
