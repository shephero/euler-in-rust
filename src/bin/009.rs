fn main() {
    'outer: for a in 0..1000 {
        'inner: for b in (a + 1)..(1000 - a) {
            let c = 1000 - a - b;
            if c <= b {continue 'inner;}
            if a * a + b * b == c * c {
                println!("{}", a * b * c);
                break 'outer;
            }
        }
    }
}
