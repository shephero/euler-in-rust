// Smallest number to be divided by numbers 1-20
fn main() {
    let mut i = 20;
    'outer: loop {
        i += 20;
        for j in 1..21 {
            if i % j != 0 {
                continue 'outer;
            }
        }
        println!("{}", i);
        break;
    }
}
