extern crate numeral;
use numeral::Numeral;

mod numplay;

fn main () {
    let num: i64 = -1111;
    println!("{} is written {}", num, num.ordinal());
    println!("{} is written {}", num, numplay::to_words(num));
}
