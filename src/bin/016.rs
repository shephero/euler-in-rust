extern crate num;
use num::bigint::BigUint;

fn main() {
    let num: BigUint = num::FromPrimitive::from_usize(2).unwrap();
    let ans = num::pow(num, 1000);
    println!("{}", ans.to_string()
                      .chars()
                      .fold(0, |acc, char| char.to_digit(10).unwrap() + acc));
}
