pub struct Triangular {
    current: u64,
    natural: u64
}

impl Iterator for Triangular {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        self.current += self.natural;
        self.natural += 1;
        Some(self.current)
    }
}

pub fn triangular() -> Triangular {
    Triangular{ current: 1, natural: 2 }
}
