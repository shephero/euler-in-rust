pub mod fibonacci;
pub use self::fibonacci::fibonacci;

pub mod prime;
pub use self::prime::{ prime_iter, primes_to };

pub mod triangular;
pub use self::triangular::triangular;

pub mod collatz;
pub use self::collatz::collatz;
