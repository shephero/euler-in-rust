pub struct Fibonacci {
    current: u64,
    previous: u64
}

impl Iterator for Fibonacci {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        let tmp = self.current;
        self.current += self.previous;
        self.previous = tmp;
        Some(self.current)
    }
}

// iterator starts at first occurance of '1'
// ie. 1, 1, 2, 3, 5...
pub fn fibonacci() -> Fibonacci {
    Fibonacci{ current: 1, previous: 0}
}
