pub struct Collatz {
    current: usize
}

impl Iterator for Collatz {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        if self.current < 2 {
            None
        } else {
            if self.current % 2 == 0 {
                self.current /= 2;
            } else {
                self.current = 3 * self.current + 1;
            }
            Some(self.current)
        }
    }
}

pub fn collatz(start: usize) -> Collatz {
    return Collatz {current: start}
}
