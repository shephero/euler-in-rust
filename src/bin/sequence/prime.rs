// Iterator over Primes
pub struct Prime {
    current: u64,
    list: Vec<u64>,
}

impl Iterator for Prime {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        let mut result = 0;
        'outer: for i in self.current.. {
            for &mut j in &mut self.list {
                let j = j;
                if i % j == 0 {
                    continue 'outer;
                }
            }
            self.list.push(i);
            self.current = i+1;
            result = i;
            break 'outer;
        }
        Some(result)
    }
}

pub fn prime_iter() -> Prime {
    Prime{ current: 2, list: Vec::new() }
}

// Sieve of eratosthenes, faster than the above iterator.
// Non-even-skipping version, because I prefer the code.
pub fn primes_to(lim: usize) -> Vec<usize> {
    let mut primes = vec!(true; lim + 1);
    primes[0 as usize] = false;
    primes[1 as usize] = false;
    for i in 0..((lim as f64).sqrt().floor() as usize) {
        if primes[i] {
            for j in 2..(lim / i + 1) {
                primes[j * i] = false;
            }
        }
    }

    primes.iter().enumerate()
        .map(|x| match x {(index, &true) => index, _ => 0})
        .filter(|x| *x != 0)
        .collect::<Vec<usize>>()
}
