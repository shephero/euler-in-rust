mod sequence;

fn main() {
    let mut start = 1;
    let mut length = 0;
    for i in 0..1000000 {
        let mut chain_length = 0;
        for _ in sequence::collatz(i) {
            chain_length += 1;
        }
        if chain_length > length {
            length = chain_length;
            start = i;
        }
    }
    println!("{}", start);
}
