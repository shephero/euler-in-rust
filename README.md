# My solutions to the [Project Euler](https://projecteuler.net/archives) challenges.

The source files are stored under src/bin.

## To run:

```bash
cargo run --release --bin {number}
```
